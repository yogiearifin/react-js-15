import { useState } from "react";

export default function Comment() {
  const [comment, setComment] = useState("");
  const [listComment, setListComment] = useState([]);
  console.log("list", listComment);
  console.log("comment", comment);
  const submitComment = () => {
    setListComment([...listComment, comment]);
  };
  return (
    <div>
      <h1>Comment</h1>
      <p>add your comment</p>
      <textarea
        style={{ resize: "none", height: "300px", width: "300px" }}
        onChange={(e) => setComment(e.target.value)}
      />
      <button onClick={submitComment}>Submit Comment </button>
      <div>
        {listComment.length
          ? listComment.map((item, index) => {
              return (
                <div key={index}>
                  <p>{index}</p>
                  <p>{item}</p>
                </div>
              );
            })
          : "No Comment"}
      </div>
    </div>
  );
}
