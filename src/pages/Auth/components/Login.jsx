import { useDispatch } from "react-redux";
import { useState } from "react";
import { LoginAction } from "../../../store/actions/auth";

export default function Login() {
  const dispatch = useDispatch();
  const [inputLogin, setInputLogin] = useState({
    email: "",
    password: "",
  });

  const changeInput = (e) => {
    setInputLogin({
      ...inputLogin,
      [e.target.name]: e.target.value,
    });
  };

  const submitLogin = () => {
    dispatch(LoginAction(inputLogin));
  };
  return (
    <div>
      <h1>Login</h1>
      <label>Username</label>
      <input
        type="text"
        placeholder="username"
        name="email"
        onChange={(e) => changeInput(e)}
      />
      <label>Password</label>
      <input
        type="password"
        placeholder="password"
        name="password"
        onChange={(e) => changeInput(e)}
      />
      <button onClick={submitLogin}>Submit</button>
    </div>
  );
}
