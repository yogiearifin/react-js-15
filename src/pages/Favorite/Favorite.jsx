import CardMovie from "../home/components/CardMovie";

export default function Favorite() {
  const fav = JSON.parse(localStorage.getItem("favorite"));
  console.log(fav);
  return (
    <div>
      <h1>Favorite Movie</h1>
      {fav.length
        ? fav.map((item, index) => {
            return (
              <div key={index} style={{ display: "flex" }}>
                <CardMovie
                  id={item.id}
                  title={item.title}
                  picture={item.picture}
                />
              </div>
            );
          })
        : "No favorite films found"}
    </div>
  );
}
