import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getMovieDetail } from "../../store/actions/movie";
import { useParams } from "react-router-dom";
import "./styles/details.scss";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

export default function Details() {
  const relative = dayjs.extend(relativeTime);
  const { id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getMovieDetail(id));
  }, []);
  const { details, loading } = useSelector((state) => state.movie.detailMovie);
  console.log(details);
  const fave = JSON.parse(localStorage.getItem("favorite"));
  const addToFavorite = () => {
    let newFav = [];
    if (localStorage.getItem("favorite") === null) {
      newFav = [];
    } else {
      newFav = JSON.parse(localStorage.getItem("favorite"));
    }
    newFav.push(details);
    localStorage.setItem("favorite", JSON.stringify(newFav));
  };

  const removeFromFavorite = () => {
    let newWatchList = JSON.parse(localStorage.getItem("favorite")).filter(
      (filter) => filter.title !== details.title
    );
    localStorage.setItem("favorite", JSON.stringify(newWatchList));
  };
  return (
    <div className="details-container">
      <h1>Detail Movie</h1>
      {loading ? (
        "loading..."
      ) : (
        <div>
          <img
            src={details.picture}
            alt={details.title}
            style={{ width: "500px" }}
          />
          <h2>{details.title}</h2>
          <p>Released Year: {details.released}</p>
          <p>
            Mock Release:
            {relative(
              dayjs(details.mock_release).format("DD/MM/YYYY")
            ).fromNow()}
          </p>
          <p>
            Time:
            {relative(
              dayjs(details.time).format("DD/MM/YYYY HH:mm")
            ).fromNow()}
          </p>
          <p>Budget: {details.budget}</p>
          <p>Rating: {details.rating}</p>
          <ul>
            {details?.actors?.length
              ? details.actors.map((item, index) => {
                  return <li key={index}>{item}</li>;
                })
              : details?.actors}
          </ul>
          {fave?.find((item) => item.title === details.title) ? (
            <button onClick={removeFromFavorite}>Remove from Favorite</button>
          ) : (
            <button onClick={addToFavorite}>Add to Favorite</button>
          )}
        </div>
      )}
    </div>
  );
}
