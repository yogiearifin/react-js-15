import { Switch, Route } from "react-router-dom";
import Home from "../pages/home/Home";
import Details from "../pages/DetailMovie/Details";
import InputMovie from "../pages/InputMovie/InputMovie";
import SearchMovie from "../pages/SearchMovie/SearchMovie";
import Auth from "../pages/Auth/Auth";
import Favorite from "../pages/Favorite/Favorite";
import Comment from "../pages/Comment/Comment";

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/movie/:id" exact>
        {/* children */}
        <Details />
      </Route>
      <Route path="/input-movie" exact component={InputMovie} />
      <Route path="/search" exact>
        <SearchMovie />
      </Route>
      <Route path="/auth/:type" exact>
        <Auth />
      </Route>
      <Route path="/favorite" exact component={Favorite} />
      <Route path="/comment" exact>
        <Comment />
      </Route>
      <Route path="*">
        <div>Not Found :(</div>
      </Route>
    </Switch>
  );
}
