// import './navbar.css'; // import style
import styles from "./navbar.module.scss";
import { Link, useHistory } from "react-router-dom";
import { useState } from "react";
import { Button } from "@mui/material";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";

const Navbar = () => {
  const [login, setLogin] = useState(false);
  const history = useHistory();
  const authorization = localStorage.getItem("token");
  const logout = () => {
    localStorage.removeItem("token"); // remove token to logout
    window.location.href = "/";
  };
  return (
    <div className={styles["container"]}>
      <a href="/">
        <h1>This is a Navbar</h1>
      </a>
      {authorization ? (
        <div className={styles.containerProfile}>
          <div className={styles.profile}>
            <p>Welcome, Yogie</p>
            <Button
              variant="outlined"
              startIcon={<ExitToAppIcon />}
              onClick={logout}
            >
              Exit
            </Button>
            <p>
              <ExitToAppIcon /> Exit
            </p>
            <ExitToAppIcon />
          </div>
          <div className={styles.dropdown}>
            <p>Profile</p>
            <p>Logout</p>
          </div>
        </div>
      ) : (
        <div className={styles.containerAuth}>
          <Link to="/auth/login">
            <p>Login</p>
          </Link>
          <Link to="/auth/register">
            <p>Register</p>
          </Link>
        </div>
      )}
    </div>
  );
};

export default Navbar;
