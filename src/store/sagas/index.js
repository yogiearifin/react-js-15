// root saga

import { all } from "redux-saga/effects";
import { watchGetMovies, watchGetMovieDetail, watchPostMovie } from "./movie";
import { watchLogin } from "./auth";

export default function* rootSaga() {
  yield all([
    watchGetMovies(),
    watchGetMovieDetail(),
    watchPostMovie(),
    watchLogin(),
  ]);
}
