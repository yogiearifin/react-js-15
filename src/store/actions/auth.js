import {
    LOGIN_BEGIN
  } from "./types";
  
  export const LoginAction = (body) => {
    return {
      type: LOGIN_BEGIN,
      body
    };
  };
  